import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		screens: [ {
				buttons: [
					{
						text: 'Next',
						url: '/age',
						type: 'submit'
					}
				]
			},
			{
				buttons: [
					{
						text: 'Back',
						url: '/',
						type: 'submit'
					},
					{
						text: 'Next',
						url: '/summary',
						type: 'submit'
					}
				]
			},{
				buttons: [
					{
						text: 'Send',
						type: 'submit'
					}
				]
			},
		],
		currentUser: {
			Name: undefined,
			Age: undefined
		}
	},
	mutations: {
		commitName(state, payload) {
			state.currentUser.Name = payload
		},
		commitAge(state, payload) {
			state.currentUser.Age = payload
		}
	},
	actions: {
		setName({commit}, name) {
			return new Promise((resolve, reject) => {
				commit('commitName', name)
				resolve()
			})
		},
		setAge({commit}, age) {
			commit('commitAge', age)
		},
		sendToServer({state}) {
			axios.post('https://0.0.0.0/API/newUser', {
				name: state.currentUser.name,
				age: state.currentUser.age
			})
			.then(response => {
				/* Do things */
				console.log('ok')
			})
			.catch(err => {
				console.log('Fake server')
				console.log(err)
				alert('Fake server')
			})
		}
	}
});
